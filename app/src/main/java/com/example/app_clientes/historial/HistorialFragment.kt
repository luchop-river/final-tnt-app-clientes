package com.example.app_clientes.historial

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app_clientes.databinding.HistorialFragmentBinding
import com.example.app_clientes.modelos.PlatoHistorial
import com.example.app_clientes.otros.UserViewModel
import com.google.firebase.database.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class HistorialFragment : Fragment() {


    private lateinit var _binding: HistorialFragmentBinding
    private val binding get() = _binding
    private val historialViewModel: HistorialViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()
    lateinit var recyclerView: RecyclerView
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDatabase()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = HistorialFragmentBinding.inflate(inflater, container, false)
        _binding.historialViewModel = historialViewModel
        _binding.lifecycleOwner = viewLifecycleOwner
        binding.chipLimpiarFavoritos.setOnClickListener { eliminar_favoritos() }
        binding.chipVaciarHistorial.setOnClickListener { eliminar_historial() }
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        this.binding.historialViewModel?.historial?.observe(viewLifecycleOwner, Observer {
                historial -> (recyclerView.adapter as HistorialAdapter).setHistorial(historial)
        })
    }

    fun initDatabase() {
        database = FirebaseDatabase.getInstance().reference
        if (!historialViewModel.historialCargado) {
            GlobalScope.launch { initHistorialUsuarioDesdeBD() }
        }

    }


    private fun initRecyclerView() {
        recyclerView = binding.historialRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = getAdapter()
    }

    private suspend fun initHistorialUsuarioDesdeBD() {
        if (historialViewModel.historialActualizado) return
        val uid = userViewModel.usuario.uid.toString()
        database.child("users").child(uid).addListenerForSingleValueEvent(
            object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.hasChild("historial")) {
                        cargarHistorialCompras(snapshot)
                        historialViewModel.historialActualizado = true
                    } else {
                        mostrarMensaje("No posees platos en tu historial")
                    }

                }
                override fun onCancelled(error: DatabaseError) {
                    mostrarMensaje("No pudimos traer tu historial")
                }
        /*database.child("users").child(uid).child("historial").addValueEventListener(object :
            ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                mostrarMensaje("No pudimos traer tu historial")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    cargarHistorialCompras(dataSnapshot)
                    if (!historialViewModel.historialCargado) {
                        historialViewModel.historialCargado = true
                    }
                }
            }*/
        })
    }

    fun getAdapter() : HistorialAdapter {

        return HistorialAdapter( object:HistorialListener{
            override fun onClickCheckboxFavorito(platoHistorial: PlatoHistorial, esFavorito: Boolean ) {
                historialViewModel.onClickCheckboxFavorito(platoHistorial, esFavorito)
                GlobalScope.launch { updateFavorito(platoHistorial, esFavorito) } //updateFavorito(platoHistorial, esFavorito)
            }
        } )
    }

    //fun  updateFavorito(plato: PlatoHistorial, esFavorito: Boolean) {
    suspend fun  updateFavorito(plato: PlatoHistorial, esFavorito: Boolean) {
        val uid = userViewModel.usuario.uid.toString()
        val idPlato = plato.idPlato.toString()
        database.child("users").child(uid).child("historial").child(idPlato).child("favorito").setValue(esFavorito)
            .addOnSuccessListener { mostrarMensaje("Favorito actualizado")}
    }

    fun eliminar_historial(){
        val uid = userViewModel.usuario.uid.toString()
        database.child("users").child(uid).child("historial").removeValue()
            .addOnSuccessListener {
                for ( i in historialViewModel.historial.value!!.indices) {
                    recyclerView.adapter!!.notifyItemRemoved(i)
                }
                mostrarMensaje("Historial eliminado")
            }
    }

    fun eliminar_favoritos(){
        val uid = userViewModel.usuario.uid.toString()
        historialViewModel.getPlatosFavoritos().forEach {
            var plato: PlatoHistorial = it
            database.child("users").child(uid).child("historial").child(it.idPlato.toString()).child("favorito").setValue(false)
                .addOnSuccessListener {
                    historialViewModel.onClickCheckboxFavorito(plato,false)
                    recyclerView.adapter!!.notifyDataSetChanged()
                }
                .addOnFailureListener { mostrarMensaje("Ha ocurrido un error eliminado los favoritos") }
        }
    }

    private fun cargarHistorialCompras(dataSnapshot: DataSnapshot) {
        val platosHistorial = dataSnapshot.children
        platosHistorial.forEach {
            val platoHistorial = it.getValue(PlatoHistorial::class.java)
            historialViewModel.addPlatoHistorial(platoHistorial as PlatoHistorial)
        }
    }

    private fun mostrarMensaje(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }
}