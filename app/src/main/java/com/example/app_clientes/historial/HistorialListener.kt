package com.example.app_clientes.historial

import com.example.app_clientes.modelos.PlatoHistorial

interface HistorialListener {
    fun onClickCheckboxFavorito(platoHistorial: PlatoHistorial, esFavorito: Boolean)
}