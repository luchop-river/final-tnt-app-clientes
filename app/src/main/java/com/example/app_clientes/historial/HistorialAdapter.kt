package com.example.app_clientes.historial

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.app_clientes.R
import com.example.app_clientes.modelos.PlatoHistorial

class HistorialAdapter(
    private val historialListener: HistorialListener
) : RecyclerView.Adapter<HistorialAdapter.ViewHolder>() {



    private lateinit var historial: List<PlatoHistorial>


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_historial_plato, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val plato = this.historial[position]
        holder.nombre.text = plato.nombre
        holder.descripcion.text = plato.descripcion
        holder.esFavorito.isChecked = plato.favorito!!
        holder.esFavorito.setOnCheckedChangeListener { _, isChecked ->
            historialListener.onClickCheckboxFavorito(plato, isChecked)
        }
    }

    internal fun setHistorial(platos: List<PlatoHistorial>) {
        this.historial = platos
    }

    override fun getItemCount(): Int = historial.size

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        private val vista: View = view
        val nombre: TextView
        val descripcion: TextView
        val esFavorito: CheckBox

        init {
            nombre = vista.findViewById(R.id.plato_historial_nombre)
            descripcion = vista.findViewById(R.id.plato_historial_descripcion)
            esFavorito = vista.findViewById(R.id.plato_historial_favorito)
        }

        override fun toString(): String {
            return super.toString() + " '" + nombre.text + "'"
        }
    }
}