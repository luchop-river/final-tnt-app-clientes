package com.example.app_clientes.historial

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app_clientes.modelos.Pedido
import com.example.app_clientes.modelos.PlatoHistorial

class HistorialViewModel : ViewModel() {
    private val _historial: MutableLiveData<MutableList<PlatoHistorial>> = MutableLiveData(mutableListOf<PlatoHistorial>())
    val historial: LiveData<MutableList<PlatoHistorial>> get() = _historial
    var notificacionDiariaCheckeada = false
    var historialCargado = false
    var historialActualizado = true

    fun onClickCheckboxFavorito( platoHistorial: PlatoHistorial, esFavorito: Boolean ) {
        var index = _historial.value?.lastIndexOf(platoHistorial) ?: return
        val nuevaLista: MutableList<PlatoHistorial>? = _historial.value
        nuevaLista?.remove(platoHistorial)
        platoHistorial.favorito = esFavorito
        index.let { nuevaLista?.add(it, platoHistorial) }
        _historial.value = nuevaLista!!
    }

    fun getPlatosFavoritos(): List<PlatoHistorial> {
        return _historial.value?.filter { p -> p.favorito as Boolean }!!
    }

    fun addPlatoHistorial(platoNuevo: PlatoHistorial) {
        if (platoExisteEnHistorial(platoNuevo)) {
            reemplazarPlato(platoNuevo)
        } else {
            encolarPlatoHistorial(platoNuevo)
        }
    }

    private fun reemplazarPlato(platoNuevo: PlatoHistorial) {
        val platoBuscadoList = _historial.value?.filter { p -> p.nombre == platoNuevo.nombre }
        val plato =  platoBuscadoList?.first()
        val index = _historial.value?.indexOf(plato)
        val nuevaLista = _historial.value
        nuevaLista?.remove(plato)
        nuevaLista?.add(index as Int, platoNuevo)
        _historial.value = nuevaLista!!
    }

    private fun encolarPlatoHistorial(platoNuevo: PlatoHistorial) {
        val tmpList = _historial.value
        tmpList?.add(platoNuevo)
        _historial.value = tmpList!!
    }

    fun transformaPedidosEnPlatosHistorial(listaPedidos: List<Pedido>): List<PlatoHistorial> {
        var listaPlatoHistorial = mutableListOf<PlatoHistorial>()
        listaPedidos.forEach { pedido ->
            val plato = traformaPedidoEnPlatoHistorial(pedido)
            if (!platoExisteEnHistorial(plato)) listaPlatoHistorial.add(plato)
        }
        return listaPlatoHistorial
    }

    private fun platoExisteEnHistorial(plato: PlatoHistorial):Boolean {
        //return _historial.value?.contains(plato)!!
        val list = _historial.value?.filter { p -> (p.nombre == plato.nombre) }
        return list?.isNotEmpty()!!
    }

    private fun traformaPedidoEnPlatoHistorial(pedido: Pedido): PlatoHistorial {
        return PlatoHistorial(
            idPlato = pedido.idPlato,
            nombre = pedido.nombre,
            descripcion = pedido.descripcion,
            favorito = false,
        )
    }

}