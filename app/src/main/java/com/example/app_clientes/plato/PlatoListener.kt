package com.example.app_clientes.plato

import com.example.app_clientes.modelos.Plato

interface PlatoListener {
    fun onVerPlatoClicked(plato: Plato)
}