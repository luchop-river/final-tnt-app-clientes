package com.example.app_clientes.plato

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.app_clientes.R
import com.example.app_clientes.modelos.Plato
import com.example.app_clientes.modelos.TipoPlato


class PlatoAdapter(var platoListener: PlatoListener): RecyclerView.Adapter<PlatoAdapter.PlatoViewHolder>() {
    private lateinit var platos: MutableList<Plato>
    private lateinit var tipoPlato: TipoPlato


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlatoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_plato_dest, parent, false)
        return PlatoViewHolder(view, platoListener)
    }

    override fun onBindViewHolder(holder: PlatoViewHolder, position: Int) {
        val plato = this.platos[position]
        holder.nombre.text = plato.nombre
        holder.precio.text = "$ ${plato.precio}"
        //holder.imagen.setImageResource(plato.imagen as Int)
        holder.btnVerPlato.setOnClickListener { platoListener.onVerPlatoClicked(plato) }

        when(tipoPlato) {
            TipoPlato.VIANDAS -> {
                val ubicacion_plato_en_semana = position + 1
                holder.nombre.text =
                    plato.nombre?.let { tipoPlato.getString(it, ubicacion_plato_en_semana) }
                holder.btnVerPlato.isEnabled = tipoPlato.isEnabled(ubicacion_plato_en_semana) && !plato.nombre.isNullOrEmpty()
            }
            else -> {
                holder.nombre.text = plato.nombre?.let { tipoPlato.getString(it) }
                holder.btnVerPlato.isEnabled = tipoPlato.isEnabled()}
        }
    }


    fun setPlatos(platos: MutableList<Plato>, tipoPlato: TipoPlato) {
        this.platos = platos
        this.tipoPlato = tipoPlato
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = platos.size

    inner class PlatoViewHolder(view: View, var platoListener: PlatoListener): RecyclerView.ViewHolder(view) {
        private val vista: View = view
        val nombre: TextView
        //var imagen: ImageView
        var precio: TextView
        val btnVerPlato: Button

        init {
            nombre = vista.findViewById(R.id.nombre_textView)
            //imagen = vista.findViewById(R.id.imagen_plato)
            precio = vista.findViewById(R.id.precio_textView)
            btnVerPlato = vista.findViewById(R.id.ver_plato_chip)
        }

        override fun toString(): String {
            return super.toString() + " '" + nombre.text + "'"
        }
    }
}