package com.example.app_clientes.plato

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app_clientes.modelos.Pedido
import com.example.app_clientes.modelos.Plato
import com.example.app_clientes.modelos.TipoPlato
import com.google.firebase.database.DataSnapshot
import java.util.*

class PlatoViewModel: ViewModel() {

        private val _platos: MutableLiveData<MutableList<Plato>> = MutableLiveData(mutableListOf<Plato>())
        val platos: LiveData<MutableList<Plato>>
            get() = _platos
        var tipoPlato: TipoPlato

        private val _platosEnCarrito: MutableLiveData<MutableList<Pedido>> = MutableLiveData(mutableListOf<Pedido>())
        val platosEnCarrito: LiveData<MutableList<Pedido>>
            get() = _platosEnCarrito

        private var platosAlmuerzo = mutableListOf<Plato>()
        private var platosDesayuno = mutableListOf<Plato>()
        private var platosMerienda = mutableListOf<Plato>()
        // Es inicializado con 5 platos vacios que representan la vianda del dia (lun. a vie.). Cuando
        // se agrega una vianda que se corresponde a un dia sera reemplazado el plato vacio de ese dia
        private var platosVianda   = mutableListOf(Plato(), Plato(),Plato(), Plato(),Plato())

        private var _platoDetalle: MutableLiveData<Plato> = MutableLiveData(Plato())
        val platoDetalle: LiveData<Plato>
            get() = _platoDetalle

        var platosCargados = false


        init {
            _platos.postValue(platosAlmuerzo)
            tipoPlato = TipoPlato.ALMUERZO
            Log.i("plato","PlatoViewModel creado")
        }

        override fun onCleared() {
            super.onCleared()
            Log.i("plato", "PlatoViewModel destruido")
        }


        fun onClickDesayuno() {
            _platos.postValue(platosDesayuno)
            tipoPlato = TipoPlato.DESAYUNO
        }

        fun onClickAlmuerzo() {
            _platos.postValue(platosAlmuerzo)
            tipoPlato = TipoPlato.ALMUERZO
        }

        fun onClickMerienda() {
            _platos.postValue(platosMerienda)
            tipoPlato = TipoPlato.MERIENDA
        }

        fun onClickViandas() {
            _platos.postValue(platosVianda)
            tipoPlato = TipoPlato.VIANDAS
        }

        fun onClickVerPlato(plato: Plato) {
            this._platoDetalle.value = plato
        }

        fun onAgregarAlCarrito(plato: Plato) {
            val pedido = trasnformarPlatoEnPedido(plato)
            agregarPedidoACarrito(pedido)
        }

        fun onAgregarPlatoDetalleAlCarrito() {
            var plato:Plato = platoDetalle.value as Plato // TODO: probar sin castear
            val pedido = trasnformarPlatoEnPedido(plato)
            agregarPedidoACarrito(pedido)
        }

        fun getMenuDelDia(): MutableList<Plato> {
            return platosAlmuerzo
        }

        private fun agregarPedidoACarrito(pedido: Pedido) {
            if (platosEnCarritoContienePedido(pedido)) return
            updatePlatosDelCarrito(pedido)
        }

        private fun updatePlatosDelCarrito(pedidoNuevo: Pedido) {
            val nuevaLista: MutableList<Pedido>? = _platosEnCarrito.value
            nuevaLista?.add(pedidoNuevo)
            updatePlatosDelCarrito(nuevaLista as MutableList<Pedido>)
        }

        fun updatePlatosDelCarrito(pedidosNuevos: MutableList<Pedido>) {
            _platosEnCarrito.value = pedidosNuevos
        }

        private fun platosEnCarritoContienePedido(pedido: Pedido): Boolean {
            val pedidoIterator = _platosEnCarrito.value?.iterator() ?: return false
            while(pedidoIterator.hasNext()){
                val pedidoListaCarrito: Pedido = pedidoIterator.next()
                if (pedido.nombre == pedidoListaCarrito.nombre ) return true
            }
            return false
        }

        private fun trasnformarPlatoEnPedido(plato: Plato): Pedido {
            return  Pedido(idPlato = plato.id, nombre = plato.nombre, descripcion = plato.descripcion, imagen =  plato.imagen, precio =  plato.precio, cantidad =  1 )
        }

        fun get_tipo_plato_pedido(p: Pedido): String {
            val plato = Plato(id = p.idPlato, nombre = p.nombre,descripcion = p.descripcion,precio = p.precio)
            if (platosAlmuerzo.contains(plato)) return "almuerzo"
            if (platosDesayuno.contains(plato)) return "desayuno"
            if (platosMerienda.contains(plato)) return "merienda"
            return "viandas"
        }

        fun addPlato(platoSnapshot: DataSnapshot) {
            val plato: Plato = getPlatoFromDataSnapshot(platoSnapshot)
            val tipoPlatoStr = platoSnapshot.child("tipo").value.toString().uppercase(Locale.getDefault())
            when (tipoPlatoStr) {
                TipoPlato.ALMUERZO.toString() -> addPlatoAlmuerzo(plato)
                TipoPlato.DESAYUNO.toString() -> addPlatoDesayuno(plato)
                TipoPlato.MERIENDA.toString() -> addPlatoMerienda(plato)
                else -> addPlatoVianda(plato, platoSnapshot.child("dia").getValue(String::class.java).toString())
            }
        }

        private fun getPlatoFromDataSnapshot(platoSnapshot: DataSnapshot): Plato {
            val id: String = platoSnapshot.key.toString()
            val nombre = platoSnapshot.child("nombre").getValue(String::class.java).toString()
            val descripcion = platoSnapshot.child("descripcion").getValue(String::class.java).toString()
            val precio= platoSnapshot.child("precio").getValue(String::class.java)?.toDouble()
            //val imagen = platoDB.child("imagen").getValue(String::class.java).toString()
            return Plato(id = id, nombre = nombre, descripcion = descripcion, precio = precio)
        }

        private fun addPlatoAlmuerzo(platoNuevo: Plato) {
            if (platosAlmuerzo.contains(platoNuevo)) return
            platosAlmuerzo.add(platoNuevo)
            if (tipoPlato == TipoPlato.ALMUERZO) _platos.postValue(platosAlmuerzo)
        }

        private fun addPlatoDesayuno(platoNuevo: Plato) {
            if (platosDesayuno.contains(platoNuevo)) return
            platosDesayuno.add(platoNuevo)
            if (tipoPlato == TipoPlato.DESAYUNO) _platos.postValue(platosDesayuno)
        }

        private fun addPlatoMerienda(platoNuevo: Plato) {
            if (platosMerienda.contains(platoNuevo)) return
            platosMerienda.add(platoNuevo)
            if (tipoPlato == TipoPlato.MERIENDA) _platos.postValue(platosMerienda)
        }

        private fun addPlatoVianda(platoNuevo: Plato, dia: String) {
            if (platosVianda.contains(platoNuevo)) return
            agregarPlatoSegunDia(platoNuevo, dia)
            if (tipoPlato == TipoPlato.VIANDAS) _platos.postValue(platosVianda)
        }

        private fun agregarPlatoSegunDia(plato: Plato, dia: String){
            val pos = when(dia) {
                "lunes"-> 0
                "martes" -> 1
                "miercoles"-> 2
                "jueves" -> 3
                else -> 4
            }
            platosVianda.removeAt(pos)
            platosVianda.add(pos, plato)
        }

    }