package com.example.app_clientes.ui

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.app_clientes.R
import com.example.app_clientes.databinding.FragmentPortadaBinding
import com.example.app_clientes.modelos.User
import com.example.app_clientes.otros.UserViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.messaging.FirebaseMessaging


private lateinit var auth: FirebaseAuth
private lateinit var database: DatabaseReference


class PortadaFragment: Fragment() {
    private lateinit var _binding: FragmentPortadaBinding
    private val binding get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        val uid = auth.currentUser?.uid.toString()
        database = FirebaseDatabase.getInstance().reference

        // Verificamos si ya estaba logueado
        if (currentUser != null) {
            // User is signed in
            // Toast.makeText(context, "Logueo Automatico", Toast.LENGTH_LONG).show()
            modeloUsuario(uid, database)
            navegarHaciaPrincipal(uid)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this._binding = FragmentPortadaBinding.inflate(inflater, container, false)
        val  view = binding.root
        binding.entrarButton.setOnClickListener { loginApp() }
        binding.registrarButton.setOnClickListener { navegarHaciaRegistrarse() }
        return view
    }

    private fun loginApp() {
        val email = _binding.email.text.toString().trim()
        val password = _binding.passw.text.toString()
        database = FirebaseDatabase.getInstance().reference

        if(email.isNotEmpty() && password.isNotEmpty()){
            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val uid = auth.currentUser?.uid.toString()
                    Log.d("TAG", "SignInUserWhitEMailPassword:Succes")
                    val user = auth.currentUser
                    Toast.makeText(context, "Se ha logueado correctamente", Toast.LENGTH_LONG).show()
                    modeloUsuario(uid, database)
                    navegarHaciaPrincipal(uid)
                } else {
                    Log.w("TAG", "SignInUserWhitEMailPassword:Failure", task.exception)
                    Toast.makeText(context, "Loguin Failed", Toast.LENGTH_LONG).show()
                }

            }
        }
    }

    private fun modeloUsuario(uid:String, database:DatabaseReference) {
        database.child("users").child(uid).addListenerForSingleValueEvent(object :
            ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val uid =
                        dataSnapshot.child("uid").getValue(
                            String::class.java
                        ).toString()
                    val nombre =
                        dataSnapshot.child("nombre").getValue(
                            String::class.java
                        ).toString()
                    val telefono =
                        dataSnapshot.child("telefono").getValue(
                            String::class.java
                        ).toString()
                    val direccion =
                        dataSnapshot.child("direccion").getValue(
                            String::class.java
                        ).toString()
                    val userViewModel: UserViewModel by activityViewModels()
                    userViewModel.usuario = User(uid = uid, nombre = nombre, telefono = telefono, direccion = direccion)

                    val favoritos = dataSnapshot.child("favoritos").children
                    favoritos.forEach { favorito -> userViewModel.add_favorito(favorito.key.toString()) }

                    //TODO: Probar quitar pedir el token de firebase y ver si andan las notificaciones
                    FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            Log.w("token", "Fetching FCM registration token failed", task.exception)
                            return@OnCompleteListener
                        }
                        userViewModel.token = task.result.toString()
                        Log.d("notif", userViewModel.token)

                    })
                }
                //ya tenemos los datos desde Firebase cargados en user:User
                //alerta(dataSnapshot)
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("ERROR", "Error de lectura")
            }
        })
    }

    private  fun alerta(data: DataSnapshot) {
        // Creo una Alerta
        val builder = AlertDialog.Builder(context)
        builder.setTitle("DATOS:")
        builder.setMessage(data.value.toString())
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun navegarHaciaPrincipal(uid: String) {
        val action = PortadaFragmentDirections.actionPortadaFragmentToNavHome(uid)
        findNavController().navigate(action)
    }

    private fun navegarHaciaRegistrarse() {
        findNavController().navigate(R.id.action_portadaFragment_to_registroFragment)
    }


}