package com.example.app_clientes.ui

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.app_clientes.databinding.FragmentRegistroBinding
import com.example.app_clientes.modelos.User
import com.example.app_clientes.otros.UserViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

private lateinit var database: DatabaseReference
private lateinit var auth: FirebaseAuth

class RegistroFragment: Fragment() {
    private lateinit var _binding: FragmentRegistroBinding
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        auth = FirebaseAuth.getInstance()
    }


    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        this._binding = FragmentRegistroBinding.inflate(inflater, container, false)
        val  view = binding.root
        binding.registrar.setOnClickListener { registrarUsuario() }
        return  view
    }


    private fun registrarUsuario() {

        val email = _binding.email.text.toString().trim()
        val password = _binding.password.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty()){
            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("TAG", "CreateUserWhitEMailPassword:Succes")
                    var user = auth.currentUser
                    Toast.makeText(context, "Su usuario ha sido creado correctamente", Toast.LENGTH_LONG).show()
                    val modeloUsuario = saveUserToFirebase()
                    alerta(modeloUsuario)
                    val userViewModel: UserViewModel by activityViewModels()
                    userViewModel.usuario = modeloUsuario
                    userViewModel.usuario.uid?.let { navegarHaciaPrincipal(it) }
                } else {
                    Log.w("TAG", "CreateUserWhitEMailPassword:Failure", task.exception)
                    Toast.makeText(context, "Authentication Failed", Toast.LENGTH_LONG).show()
                }

            }
        }
    }

    private fun saveUserToFirebase(): User {
        database = FirebaseDatabase.getInstance().reference
        val nombre = _binding.nombre.text.toString()
        val direccion = _binding.direccion.text.toString()
        val telefono = _binding.telefono.text.toString()
        val uid = auth.currentUser?.uid.toString()

        val user =
            User(uid = uid, nombre =  nombre, direccion =  direccion, telefono =  telefono)

        database.child("users").child(uid).setValue(user)

        return user
    }

    private  fun alerta(value: User) {
        // Creo una Alerta
        val nombre = value.nombre
        val uid = value.uid
        val telefono = value.telefono
        val direccion = value.direccion

        val builder = AlertDialog.Builder(context)
        builder.setTitle("DATOS:")
        builder.setMessage("$uid $nombre $direccion $telefono")
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun navegarHaciaPrincipal(uid: String) {
        val action = RegistroFragmentDirections.actionRegistroFragmentToNavHome(uid)
        findNavController().navigate(action)
    }


}