package com.example.app_clientes.ui

import android.app.NotificationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.app_clientes.databinding.FragmentCompraBinding
import com.example.app_clientes.historial.HistorialViewModel
import com.example.app_clientes.modelos.Compra
import com.example.app_clientes.modelos.Ingrediente
import com.example.app_clientes.modelos.Pedido
import com.example.app_clientes.modelos.PlatoHistorial
import com.example.app_clientes.otros.CompraViewModel
import com.example.app_clientes.otros.UserViewModel
import com.example.app_clientes.plato.PlatoViewModel
import com.example.app_clientes.sendNotification
import com.google.firebase.database.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class CompraFragment : Fragment() {


    private lateinit var _binding: FragmentCompraBinding
    private val binding get() = _binding
    private val compraViewModel: CompraViewModel by activityViewModels()
    private lateinit var tipo_entrega: String
    private var costo_entrega: Double = 0.00
    private lateinit var precio_compra: String
    private lateinit var database: DatabaseReference
    private val historialViewModel: HistorialViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        database = FirebaseDatabase.getInstance().reference
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this._binding = FragmentCompraBinding.inflate(inflater, container, false)
        binding.compraViewModel = compraViewModel
        tipo_entrega = binding.compraViewModel?.compra?.value?.getEntregaStr()!!
        costo_entrega = binding.compraViewModel?.compra?.value?.getCostoEntrega() !!
        precio_compra = binding.compraViewModel?.compra?.value?.precio.toString()
        //_binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.tipoEntregaTextView.text = "Tipo de entrega: ${tipo_entrega}"
        binding.totalCompraTextView.text = "Total: $${precio_compra}"

        if (costo_entrega > 0) {
            binding.costoEntregaTextView.text = "Costo de entrega: ${costo_entrega.toString()}"
        } else {
            binding.costoEntregaTextView.text = ""
        }

        binding.chipConfirmarCompra.setOnClickListener { realizarCompra() }
    }
    private fun realizarCompra() {
        escribirPedidoEnBD()
    }

    private fun escribirPedidoEnBD() {
        val key = database.child("pedidos").push().key
        if (key == null) {
            onCompraFallida("Tu compra no se pudo realizar correctamente")
            return
        }
        val compra_confirmada: Compra = binding.compraViewModel?.get_compra() !!
        database.child("pedidos").child(key).setValue(compra_confirmada)
            .addOnFailureListener { onCompraFallida("Tu compra no se pudo realizar correctamente") }
            .addOnSuccessListener {
                agregarCompraAlHistorial()
                GlobalScope.launch {  decrementar_stock_ingredientes_por_compra() }
                GlobalScope.launch {  escucharNotificacionPedido(key) }
                navegar_hacia_home()
            }
    }

    private fun decrementar_stock_ingredientes_por_compra() {
        val pedidos = binding.compraViewModel?.get_pedidos()
        // Por cada pedido (plato + cant_porciones) de la compra
        pedidos?.forEach { pedido ->
            val tipo_pedido = get_tipo_pedido(pedido)
            val id_plato = pedido.idPlato.toString()
            // obtengo la referencia a los ingredientes del palto (plato_id -> lista_ingredientes)
            var ref = database.child("menus").child(tipo_pedido).child(id_plato).child("ingredientes").ref
            if (tipo_pedido == "viandas") {
                ref = database.child("viandas").child(id_plato).child("ingredientes").ref
            }
            // le paso la referencia a los ingredientes del plato y la cantidad de platos del pedido
            pedido.cantidad?.let { qty_platos -> decrementar_stock_ingredientes_en_cantidad(ref, qty_platos) }
        }
    }

    private fun get_tipo_pedido(pedido: Pedido):String {
        val platoViewModel: PlatoViewModel by activityViewModels()
        return platoViewModel.get_tipo_plato_pedido(pedido)
    }


    private fun decrementar_stock_ingredientes_en_cantidad(refIngredientesPlato: DatabaseReference, cantidad_platos: Int) {
        refIngredientesPlato.addListenerForSingleValueEvent( object : ValueEventListener {
            override fun onDataChange(snapshotIngredientes: DataSnapshot) {
                // Por cada ingrediente de un plato
                snapshotIngredientes.children.forEach {
                    val ingrediente = get_ingrediente_from_snap(it)
                    val cant_ingredientes_a_decrementar = cantidad_platos * ingrediente.cantidad
                    // obtengo la referencia a quantity del ingrediente
                    val ref = database.child("ingredientes").child(ingrediente.id.toString()).child("quantity")
                    ref.get().addOnSuccessListener { stock_ingrediente ->
                        val stock_nuevo = stock_ingrediente.value.toString().toInt()  - cant_ingredientes_a_decrementar
                        ref.setValue(stock_nuevo)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                return
            }

        })
    }

    private fun get_ingrediente_from_snap(snap: DataSnapshot?): Ingrediente {
        val id = snap?.key.toString()
        val nombre = snap?.child("name")?.value.toString()
        val cant = snap?.child("quantity")?.value.toString().toInt()
        return Ingrediente(nombre = nombre, cantidad = cant, id=id)
    }

    private fun navegar_hacia_home() {
        findNavController().navigate(com.example.app_clientes.R.id.action_compraFragment_to_nav_home)
    }

    private fun escucharNotificacionPedido(idCompra: String) {
        val ref = database.child("pedidos").child(idCompra).child("completo")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                return
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val pedidoCocinado: Boolean = dataSnapshot.value as Boolean
                if (pedidoCocinado) {
                    ref.removeEventListener(this)
                    notificar("Tu pedido ya esta listo!")
                }
            }
        })
    }


    private fun notificar(mensaje: String) {
        val notificationManager: NotificationManager? = ContextCompat.getSystemService(requireContext(), NotificationManager::class.java)
        notificationManager?.sendNotification(mensaje,requireContext())
    }



    private fun agregarCompraAlHistorial() {
        val platosComprados = binding.compraViewModel!!.get_pedidos()
        val listaPlatosParaAgregarAlHistorial = historialViewModel.transformaPedidosEnPlatosHistorial(platosComprados)
        if (listaPlatosParaAgregarAlHistorial.isEmpty()){
            onCompraExitosa()
        } else {
            agregarPlatosAHistorialUsuario(listaPlatosParaAgregarAlHistorial)
        }
    }

    private fun agregarPlatosAHistorialUsuario(platos: List<PlatoHistorial>) {
        historialViewModel.historialActualizado = false
        platos.forEach { plato ->
            database.child("users").child(userViewModel.usuario.uid.toString()).child("historial").child(plato.idPlato.toString()).setValue(plato)
                .addOnFailureListener { onCompraFallida("Tu compra fue realizada con éxito! No pudimos actualizar tu historial") }
                .addOnSuccessListener { onCompraExitosa() }
        }
    }

    private fun onCompraExitosa() {
        // Navegar al Home
        Toast.makeText(context, "Tu compra fue realizada con éxito", Toast.LENGTH_LONG).show()
    }

    private fun onCompraFallida(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }



}