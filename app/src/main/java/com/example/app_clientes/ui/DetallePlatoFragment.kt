package com.example.app_clientes.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.app_clientes.databinding.FragmentDetallePlatoDestBinding
import com.example.app_clientes.historial.HistorialViewModel
import com.example.app_clientes.modelos.Plato
import com.example.app_clientes.plato.PlatoViewModel
import com.google.firebase.storage.FirebaseStorage

class DetallePlatoFragment: Fragment() {
    private lateinit var _binding: FragmentDetallePlatoDestBinding
    private val binding get() = _binding
    lateinit var recyclerView : RecyclerView
    private val platoViewModel: PlatoViewModel by activityViewModels()
    private val historialViewModel: HistorialViewModel by activityViewModels()
    //private lateinit var database: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this._binding = FragmentDetallePlatoDestBinding.inflate( inflater, container, false)
        binding.platoViewModel = platoViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.agregarAlCarritoChip.setOnClickListener { platoViewModel.onAgregarPlatoDetalleAlCarrito() }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        platoViewModel.platoDetalle.observe(viewLifecycleOwner, Observer { plato ->
            actualizar_ui(plato)
        })
    }

    fun actualizar_ui(plato: Plato) {
        binding.nombreTextView.setText(plato.nombre)
        binding.descripcionTextView.setText(plato.descripcion)
        var precio_str = plato.precio.toString()
        binding.precioTextView.setText(precio_str)
        var idPlato = plato.id

        FirebaseStorage.getInstance().reference.child("images/$idPlato").downloadUrl.addOnSuccessListener { uri ->

            Glide
                .with(context)
                .load(uri)
                .into(binding.imagenPlato)

        }. addOnFailureListener {
            Toast.makeText(context, "No se pudo visualizar el Plato", Toast.LENGTH_SHORT).show()
        }


    }







}