package com.example.app_clientes.ui.home

import android.app.NotificationChannel
import android.app.NotificationManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app_clientes.R
import com.example.app_clientes.databinding.FragmentHomeBinding
import com.example.app_clientes.historial.HistorialViewModel
import com.example.app_clientes.modelos.Plato
import com.example.app_clientes.modelos.PlatoHistorial
import com.example.app_clientes.modelos.TipoPlato
import com.example.app_clientes.plato.PlatoAdapter
import com.example.app_clientes.plato.PlatoListener
import com.example.app_clientes.plato.PlatoViewModel
import com.google.firebase.database.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    lateinit var recyclerView : RecyclerView
    private val platoViewModel: PlatoViewModel by activityViewModels()
    private val historialViewModel: HistorialViewModel by activityViewModels()
    private lateinit var database: DatabaseReference
    val args: HomeFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDatabase()
        crearCanal(getString(R.string.channel_id),getString(R.string.nombre_channel_id))
    }


    private fun crearCanal(idCanal: String, nombreCanal: String) {
        // Crear canal de notificacion para versiones superiores a API 26.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val canalNotificacion = NotificationChannel(
                idCanal,
                nombreCanal,
                NotificationManager.IMPORTANCE_HIGH
            )
                .apply {
                    setShowBadge(true)
                    enableLights(true)
                    lightColor = Color.RED
                    enableVibration(true)
                    description = "Canal de notificación de La cocina de Juan"
                }
            // Registrar el canal
            val notificationManager: NotificationManager? =
                ContextCompat.getSystemService(requireContext(), NotificationManager::class.java)
            notificationManager?.createNotificationChannel(canalNotificacion)
        }
    }


    fun initDatabase() {
        database = FirebaseDatabase.getInstance().reference
        GlobalScope.launch { initPlatosDesdeBD() }
        if (!historialViewModel.historialCargado) GlobalScope.launch { initHistorialUsuarioDesdeBD() }
        GlobalScope.launch { initViandasDesdeBD() }
    }



    suspend private fun initPlatosDesdeBD() {
        database.child("menus").addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("DBFirebase", "Error de lectura: $databaseError")
                mostrarMensaje("No pudimos traer los platos. Vuelve a iniciar al app")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    crearListaPlatos(dataSnapshot)
                    platoViewModel.platosCargados = true
                    //checkearNotificacionMenuDiario()
                }
            }
        })
    }

    private suspend fun initHistorialUsuarioDesdeBD() {
        if (args.uid == null) return
        val uid = args.uid
        database.child("users").child(uid).addListenerForSingleValueEvent(
            object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.hasChild("historial")) {
                        cargarHistorialCompras(snapshot.child("historial"))
                        //comprobarFavoritos()
                    }
                    historialViewModel.historialCargado = true

                }

                override fun onCancelled(error: DatabaseError) {
                    mostrarMensaje("No pudimos traer tu historial")
                }
            }
        )
        /*database.child("users").child(uid).child("historial").addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(databaseError: DatabaseError) {
                mostrarMensaje("No pudimos traer tu historial")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    cargarHistorialCompras(dataSnapshot)
                    //comprobarFavoritos()
                    if (!historialViewModel.historialCargado) {
                        historialViewModel.historialCargado = true
                    }
                }
            }
        })*/
    }

    private fun comprobarFavoritos() {
        //TODO: Implementar: por cada favorito del historial -> verificar si esta en platos
    }


    private fun initViandasDesdeBD() {
        val fechaInicio = getFechaInicioViandasStr()
        val fechaTope = getFechaTopeViandasStr()
        database.child("viandas").orderByChild("fecha").startAt(fechaInicio).endAt(fechaTope).limitToFirst(5).addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onCancelled(databaseError: DatabaseError) {
                mostrarMensaje("No pudimos traer las viandas. Vuelve a iniciar al app")
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    crearListaViandas(dataSnapshot)
                }
            }
        })
    }


    private fun mostrarMensaje(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }


    fun crearListaPlatos(dataSnapshot: DataSnapshot): Boolean {
        val tipoPlato = dataSnapshot.children
        val hoy = getFechaHoyStr()
        tipoPlato.forEach {
            when (it.key?.toUpperCase()) {
                TipoPlato.ALMUERZO.toString() -> {
                    val platosAlmuerzo = it.children.filter { plato -> plato.child("fecha").value == hoy }
                    platosAlmuerzo.forEach {platoSnapshot ->
                        if (platoSnapshot.exists()) platoViewModel.addPlato(platoSnapshot)
                    }
                }
                else -> {
                    it.children.forEach {platoSnapshot ->
                        if (platoSnapshot.exists()) platoViewModel.addPlato(platoSnapshot)
                    }
                }
            }
        }
        return true
    }

    private fun crearListaViandas(dataSnapshot: DataSnapshot) {
        //Log.i("platos","==>VIANDAS con fechas calculadas >>> "+dataSnapshot.toString())
        val viandas = dataSnapshot.children
        viandas.forEach {platoSnapshot ->
            if(platoSnapshot.exists()) platoViewModel.addPlato(platoSnapshot)
        }

    }

    private fun getFechaHoyStr(): String {
        val sdf = SimpleDateFormat("dd/M/yyyy", Locale.getDefault())
        val currentDate = sdf.format(Date())
        return currentDate
    }


    private fun getFechaInicioViandasStr():String {
        val sdf = SimpleDateFormat("dd/M/yyyy")
        var cal = Calendar.getInstance()
        val delta = getDeltaTopeInicio()
        cal.add(Calendar.DAY_OF_WEEK, delta)
        return sdf.format(cal.time)
    }

    private fun getFechaTopeViandasStr():String {
        val CORRIMIENTO_DIAS = 1
        val sdf = SimpleDateFormat("dd/M/yyyy")
        var cal = Calendar.getInstance()
        val delta = getDeltaTopeInicio() + CORRIMIENTO_DIAS
        cal.add(Calendar.DAY_OF_WEEK, delta)
        return sdf.format(cal.time)
    }

    private fun getDeltaTopeInicio(): Int {
        val hoy =  Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        return when(hoy) {
            7 -> 0 //sabado, no restes dias
            else -> -1 * hoy
        }
    }


    private fun cargarHistorialCompras(dataSnapshot: DataSnapshot) {
        val platosHistorial = dataSnapshot.children
        platosHistorial.forEach {
            val platoHistorial = it.getValue(PlatoHistorial::class.java)
            historialViewModel.addPlatoHistorial(platoHistorial as PlatoHistorial)
        }
    }





    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        this._binding = FragmentHomeBinding.inflate(inflater, container, false)
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        binding.platoViewModel = platoViewModel
        realizarBindings()
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        platoViewModel.platos.observe(viewLifecycleOwner, Observer { platos ->
            (recyclerView.adapter as PlatoAdapter).setPlatos(platos, platoViewModel.tipoPlato)
        })
    }

    private fun initRecyclerView() {
        recyclerView = binding.homeRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = getPlatoAdapter()
    }

    private fun getPlatoAdapter(): PlatoAdapter {
        return PlatoAdapter(object: PlatoListener {

            override fun onVerPlatoClicked(plato: Plato) {
                Toast.makeText(context, "Click plato",Toast.LENGTH_LONG )
                binding.platoViewModel?.onClickVerPlato(plato)
                navegarHaciaDetallePlato()
            }
        })
    }



    private fun realizarBindings(){
        binding.fab.setOnClickListener{ navegarHaciaCarrito()}
        binding.btnAlmuerzo.setOnClickListener { platoViewModel.onClickAlmuerzo() }
        binding.btnViandas.setOnClickListener { platoViewModel.onClickViandas() }
        binding.btnDesayuno.setOnClickListener { platoViewModel.onClickDesayuno() }
        binding.btnMerienda.setOnClickListener { platoViewModel.onClickMerienda() }
    }

    private fun navegarHaciaDetallePlato() {
        findNavController().navigate(R.id.action_nav_home_to_detallePlatoFragment)
    }
    private fun navegarHaciaCarrito() {
        findNavController().navigate(R.id.action_nav_home_to_carritoFragment)
    }
}