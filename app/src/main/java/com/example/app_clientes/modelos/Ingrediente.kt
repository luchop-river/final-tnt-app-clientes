package com.example.app_clientes.modelos

class Ingrediente  (
    var nombre:String? = "",
    var cantidad:Int = 0,
    var id:String? = ""
) {
    override fun toString(): String {
        return "${nombre}"
    }
}