package com.example.app_clientes.modelos

enum class TipoEntregaPedido {
    DELIVERY_A_CASA,
    DELIVERY_A_OFICINA,
    RETIRO_EN_LUGAR
}