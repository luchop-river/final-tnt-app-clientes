package com.example.app_clientes.modelos

import java.util.*

enum class TipoPlato {
    DESAYUNO {
        override fun getString(nombre_plato: String,  ubicacion_plato_en_semana:Int ): String = "Desayuno: $nombre_plato"

        override fun isEnabled(ubicacion_plato_en_semana: Int): Boolean {
            val HORA_MAX_DESAYUNO = 12
            return ( Calendar.getInstance().get(Calendar.HOUR_OF_DAY) < HORA_MAX_DESAYUNO)
        }
    },
    ALMUERZO {
        override fun getString(nombre_plato: String, ubicacion_plato_en_semana:Int) = "$nombre_plato"

        override fun isEnabled(ubicacion_plato_en_semana: Int): Boolean {
            val HORA_MAX_ALMUERZO = 15
            return ( Calendar.getInstance().get(Calendar.HOUR_OF_DAY) < HORA_MAX_ALMUERZO)
        }
    },
    MERIENDA {
        override fun getString(nombre_plato: String, ubicacion_plato_en_semana: Int): String = "$nombre_plato"

        override fun isEnabled(ubicacion_plato_en_semana: Int): Boolean {
            val HORA_MIN_MERIENDA = 15
            val HORA_MAX_MERIENDA = 18
            return (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) in HORA_MIN_MERIENDA until HORA_MAX_MERIENDA)
        }
    },
    VIANDAS  {
        override fun getString(nombre_plato: String, ubicacion_plato_en_semana: Int): String {
            val msgPlatoSinNombre = "Aún no hemos decidido el plato del dia"
            val dia = when (ubicacion_plato_en_semana) {
                1 -> "Lunes"
                2 -> "Martes"
                3 -> "Miercoles"
                4 -> "Jueves"
                else -> "Viernes"
            }
            return if (nombre_plato.isNullOrEmpty()) "$msgPlatoSinNombre $dia"  else "$dia: $nombre_plato"
        }

        override fun isEnabled(ubicacion_plato_en_semana: Int): Boolean {
            // DAY_OF_WEEK esta entre [1..7] (SUNDAY, MONDAY, .. ,FRIDAY,SATURDAY)
            val hoy: Int = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
            if (esFinDeSemana(hoy)) return true
            return when(ubicacion_plato_en_semana) {
                2 -> esAnteriorAMartes(hoy)
                3 -> esAnteriorAMiercoles(hoy)
                4 -> esAnteriorAJueves(hoy)
                5 -> esAnteriorAViernes(hoy)
                else -> false // es lunes (ubicacion_plato_en_semana == 1)
            }
        }

        private fun esFinDeSemana(dia: Int):Boolean = (dia == 1 || dia ==7)
        private fun esAnteriorAMartes(dia: Int):Boolean = (dia < 3)
        private fun esAnteriorAMiercoles(dia: Int):Boolean = (dia < 4)
        private fun esAnteriorAJueves(dia: Int):Boolean = (dia < 5)
        private fun esAnteriorAViernes(dia: Int):Boolean = (dia < 6)

    };

    abstract fun isEnabled(ubicacion_plato_en_semana: Int = 0): Boolean
    abstract fun getString(nombre_plato: String, ubicacion_plato_en_semana: Int = 0): String
}