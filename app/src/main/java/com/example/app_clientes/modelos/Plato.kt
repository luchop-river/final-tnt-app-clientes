package com.example.app_clientes.modelos

data class Plato(
    var id: String? = "",
    val nombre: String? = "",
    val descripcion: String? = "",
    val precio: Double? = 0.00,
    val tipo: String? = "",
    val imagen: Int? = 0){ }