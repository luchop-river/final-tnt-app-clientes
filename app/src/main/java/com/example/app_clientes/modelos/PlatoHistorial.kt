package com.example.app_clientes.modelos

class PlatoHistorial (
    val idPlato: String? = "",
    val nombre: String? = "",
    val descripcion: String? = "",
    var favorito: Boolean? = false,
    ){

    //@Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "idPlato" to idPlato,
            "nombre" to nombre,
            "descripcion" to descripcion,
            "favorito" to favorito,
        )
    }
}