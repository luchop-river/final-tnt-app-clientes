package com.example.app_clientes.modelos

//import com.google.firebase.database.Exclude
val COSTO_DELIVERY_A_CASA = 100.00
val COSTO_DELIVERY_A_OFICINA = 50.00

class Compra(pedidos: List<Pedido>, user: User, tipoEntregaPedido: TipoEntregaPedido = TipoEntregaPedido.RETIRO_EN_LUGAR) {

    var listaPlatos: MutableMap<String,Int> = mutableMapOf()
    var precio: Double = 0.00
    var tipoEntrega = TipoEntregaPedido.RETIRO_EN_LUGAR
    var nombreCliente: String = ""
    var direccionEntrega: String = ""
    var completo = false



    init {
        pedidos.forEach { pedido ->
            listaPlatos.put(pedido.nombre.toString(), pedido.cantidad!!)
            precio += pedido.precio?.times(pedido.cantidad!!)!!
        }
        nombreCliente = user.nombre.toString()
        setEntrega(tipoEntregaPedido, user)
    }

    fun setEntrega (tipo: TipoEntregaPedido, user: User) {
        tipoEntrega = tipo
        when (tipoEntrega) {
            TipoEntregaPedido.DELIVERY_A_CASA -> {
                precio += COSTO_DELIVERY_A_CASA
                direccionEntrega = user.direccion.toString()
            }
            TipoEntregaPedido.DELIVERY_A_OFICINA -> {
                precio += COSTO_DELIVERY_A_OFICINA
                direccionEntrega = user.oficina.toString()
            }
            else -> precio

        }
    }

    fun getEntregaStr(): String {
        var entrega:String
        when (this.tipoEntrega) {
            TipoEntregaPedido.DELIVERY_A_CASA -> entrega = "Delivery a casa"
            TipoEntregaPedido.DELIVERY_A_OFICINA -> entrega = "Delivery a oficina"
            else -> entrega = "Paso a buscar"
        }
        return entrega
    }

    fun getCostoEntrega(): Double {
        var costo_entrega:Double = 0.00
        if (this.tipoEntrega == TipoEntregaPedido.DELIVERY_A_CASA) {
            costo_entrega = COSTO_DELIVERY_A_CASA
        } else if (this.tipoEntrega ==  TipoEntregaPedido.DELIVERY_A_OFICINA) {
            costo_entrega = COSTO_DELIVERY_A_OFICINA
        }
        return costo_entrega
    }


    //@Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
            "listaPlatos" to listaPlatos,
            "precio" to precio,
            "tipoEntrega" to tipoEntrega,
            "completo" to completo
        )
    }
}