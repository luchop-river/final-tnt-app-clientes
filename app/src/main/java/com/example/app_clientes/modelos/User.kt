package com.example.app_clientes.modelos

class User(
    var direccion:String? = "",
    var nombre:String? = "",
    var telefono:String? = "",
    var uid:String? = "",
    var oficina:String? = ""
)
