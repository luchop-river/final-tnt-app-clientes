package com.example.app_clientes.modelos

val PASTAS = "PASTAS"
val INTERNACIONAL = "INTERNACIONAL"
val DIETA = "DIETA"
val PARRILLA = "PARRILLA"

class ComidaFavorita {
    var nombre: String = ""
    var esFavorita:Boolean = false

    constructor(nombre: String, esFavorita: Boolean) {
        this.nombre = nombre
        this.esFavorita = esFavorita
    }
}