package com.example.app_clientes.modelos

class Pedido(
    val idPlato: String? = "",
    val nombre: String? = "",
    val precio: Double? = 0.00,
    val imagen: Int? = 0,
    var cantidad: Int? = 1,
    var descripcion: String? = ""
) {}
