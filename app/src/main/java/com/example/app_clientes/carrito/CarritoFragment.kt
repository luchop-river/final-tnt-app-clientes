package com.example.app_clientes.carrito

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app_clientes.R
import com.example.app_clientes.databinding.CarritoFragmentBinding
import com.example.app_clientes.modelos.COSTO_DELIVERY_A_CASA
import com.example.app_clientes.modelos.COSTO_DELIVERY_A_OFICINA
import com.example.app_clientes.modelos.Pedido
import com.example.app_clientes.otros.CompraViewModel
import com.example.app_clientes.otros.UserViewModel
import com.example.app_clientes.plato.PlatoViewModel

class CarritoFragment : Fragment() {

    private lateinit var _binding: CarritoFragmentBinding
    private val binding get() = _binding
    private val platoViewModel : PlatoViewModel by activityViewModels()
    private val userViewModel: UserViewModel by activityViewModels()

    lateinit var recyclerView : RecyclerView
    //private lateinit var database: DatabaseReference
    private val compraViewModel: CompraViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       // database = FirebaseDatabase.getInstance().reference
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = CarritoFragmentBinding.inflate(inflater, container, false)

        val platosEnCarrito : MutableList<Pedido> = platoViewModel.platosEnCarrito.value as MutableList<Pedido>
        val carritoViewModel : CarritoViewModel by viewModels { CarritoViewModelFactory(( platosEnCarrito ) ) }
        carritoViewModel.user = userViewModel.usuario
        _binding.carritoViewModel = carritoViewModel
        _binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }


   override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecyclerView()
        binding.carritoViewModel?.pedidos?.observe(viewLifecycleOwner, Observer { pedidos ->
            (recyclerView.adapter as CarritoAdapter).setPedidos(pedidos)
            platoViewModel.updatePlatosDelCarrito(pedidos)
            atualizar_precio_compra()
        })
        binding.comprarButton.setOnClickListener { realizarCompra() }
        binding.opcionesEnvioRadioGrouop.setOnCheckedChangeListener { group, checkedId -> atualizar_precio_compra() }
        //binding.deliveryACasaRadioButton.setOnClickListener { atualizar_precio_compra() }
        //binding.deliveryAOficinaRadioButton.setOnClickListener { atualizar_precio_compra() }
        //binding.retiroEnLugarRadioButton.setOnClickListener { atualizar_precio_compra() }
   }

    private fun initRecyclerView() {
        recyclerView = binding.listaCarritoRecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = getCarritoAdapter()
    }

    private fun realizarCompra() {
        binding.carritoViewModel?.onClickComprar()
        if (binding.carritoViewModel?.pudeArmarCompra() as Boolean) {
            val c = this.binding.carritoViewModel?.get_compra()
            compraViewModel.set_compra(c!!)
            val pedidos = this.binding.carritoViewModel?.getPedidosConCantidadMayorACero()
            compraViewModel.set_pedidos(pedidos!!)
            findNavController().navigate(R.id.action_carritoFragment_to_compraFragment)
        }
    }



    private fun getCarritoAdapter(): CarritoAdapter {
        return CarritoAdapter(object:CarritoClickListener{
            override fun onIncrementarCantidadProductoClicked(producto: Pedido) {
                binding.carritoViewModel?.onIncrementarCantidadProducto(producto)
                habilitarBotonesCompra()
            }

            override fun onRestarCantidadProductoClicked(producto: Pedido) {
                binding.carritoViewModel?.onRestarCantidadProducto(producto)
                habilitarBotonesCompra()
            }
        })
    }

    fun atualizar_precio_compra() {
        val precio_pedido = binding.carritoViewModel?.get_precio_platos_carrito()
        val precio = if (precio_pedido == 0.0) 0.0 else get_precio_envio() + precio_pedido!!
        binding.precioTotalCompraTextView.text = "Total: ${precio}"
    }

    private fun get_precio_envio(): Double{
        if (binding.deliveryACasaRadioButton.isChecked) return COSTO_DELIVERY_A_CASA
        if (binding.deliveryAOficinaRadioButton.isChecked) return COSTO_DELIVERY_A_OFICINA
        return 0.0
    }

    fun habilitarBotonesCompra() {
        binding.comprarButton.isEnabled = binding.carritoViewModel?.estaHabilitadoParaComprar()!!
        binding.limpiarCarritoButton.isEnabled = binding.carritoViewModel?.puedoLimpiarCarrito()!!
    }


}