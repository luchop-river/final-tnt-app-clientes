package com.example.app_clientes.carrito

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.app_clientes.R
import com.example.app_clientes.modelos.Pedido

class CarritoAdapter(
    var listenerCantidadProductoButtons: CarritoClickListener
): RecyclerView.Adapter<CarritoAdapter.CarritoViewHolder>()  {


    private lateinit var pedidos: MutableList<Pedido>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarritoViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_lista_carrito, parent, false)
        return CarritoViewHolder(view,listenerCantidadProductoButtons)
    }

    override fun onBindViewHolder(holder: CarritoViewHolder, posicionProducto: Int) {
        val productoPedido: Pedido = this.pedidos[posicionProducto]
        holder.nombre.text = productoPedido.nombre
        holder.cantidad.text = productoPedido.cantidad.toString()
        holder.precio.text = "$ ${productoPedido.precio}"
        holder.imagen.setImageResource(productoPedido.imagen as Int)
        holder.btnIncrementarCantidad.setOnClickListener {
            listenerCantidadProductoButtons.onIncrementarCantidadProductoClicked(productoPedido)
        }
        holder.btnRestarCantidad.setOnClickListener {
            listenerCantidadProductoButtons.onRestarCantidadProductoClicked(productoPedido)
        }
    }

    override fun getItemCount(): Int = pedidos.size

    internal fun setPedidos(listaPedidos: MutableList<Pedido>) {
        this.pedidos = listaPedidos
        notifyDataSetChanged()
    }


    inner class CarritoViewHolder(view: View, listener: CarritoClickListener): RecyclerView.ViewHolder(view) {
        private val vista: View = view
        var nombre: TextView
        var precio: TextView
        var imagen: ImageView
        var cantidad: TextView
        var btnIncrementarCantidad: Button
        var btnRestarCantidad: Button
        init {
            nombre = vista.findViewById(R.id.pedido_nombrePlato)
            precio = vista.findViewById(R.id.pedido_precioUnidad)
            imagen = vista.findViewById(R.id.pedido_ImageView)
            cantidad = vista.findViewById(R.id.pedido_cantidadProducto)
            btnIncrementarCantidad = vista.findViewById(R.id.incrementarCantidad_buttonPedido)
            btnRestarCantidad = vista.findViewById(R.id.restarCantidad_buttonPedido)
        }
    }

}