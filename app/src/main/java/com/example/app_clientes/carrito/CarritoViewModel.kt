package com.example.app_clientes.carrito

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app_clientes.modelos.Compra
import com.example.app_clientes.modelos.Pedido
import com.example.app_clientes.modelos.TipoEntregaPedido
import com.example.app_clientes.modelos.User

class CarritoViewModel(private val platos: MutableList<Pedido>) : ViewModel() {

    private var _pedidos: MutableLiveData<MutableList<Pedido>> = MutableLiveData(mutableListOf<Pedido>())
    val pedidos: LiveData<MutableList<Pedido>>
        get() = _pedidos

    private var tipoEntregaPedido = TipoEntregaPedido.RETIRO_EN_LUGAR
    private val UNIDAD_PRODUCTO = 1

    lateinit var compra: Compra
    lateinit var user: User


    init {
        _pedidos.postValue(platos)
    }

    fun estaHabilitadoParaComprar(): Boolean {
        return (getPedidosConCantidadMayorACero().isNotEmpty())
    }
    fun puedoLimpiarCarrito(): Boolean {
        return pedidos.value?.isNotEmpty()!!
    }

    fun onClickComprar() {
        armarCompra()
    }

    fun onClickLimpiarCarrito() {
        limpiarCarrito()
    }

    fun onIncrementarCantidadProducto(productoPedido: Pedido) {
        val posicionAntigua: Int = _pedidos.value?.lastIndexOf(productoPedido) ?: return
        val nuevaListaCarrito: MutableList<Pedido> = removerPedidoDelCarrito(productoPedido)
        productoPedido.cantidad = productoPedido.cantidad?.plus(UNIDAD_PRODUCTO)
        nuevaListaCarrito.add(posicionAntigua, productoPedido)
        updatePedidos(nuevaListaCarrito)
    }

    fun onRestarCantidadProducto(productoPedido: Pedido) {
        val posicionAntigua: Int = pedidos.value?.lastIndexOf(productoPedido) ?: return
        val nuevaLista: MutableList<Pedido> = removerPedidoDelCarrito(productoPedido)
        if (productoPedido.cantidad!! > 0) {
            productoPedido.cantidad = productoPedido.cantidad!!.minus(UNIDAD_PRODUCTO)
            nuevaLista.add(posicionAntigua, productoPedido)
        }
        updatePedidos(nuevaLista)
    }

    fun setearTipoEntregaPedido(tipoEntrega: TipoEntregaPedido) {
        tipoEntregaPedido = tipoEntrega
    }

    private fun armarCompra() {
        val pedidosValidos = getPedidosConCantidadMayorACero()
        if (pedidosValidos.isEmpty()) return
        compra = Compra(pedidosValidos, user, tipoEntregaPedido)
    }

    fun pudeArmarCompra(): Boolean {
        return (this::compra.isInitialized)
    }


    private fun limpiarCarrito() {
        _pedidos.value?.clear()
        _pedidos.value = _pedidos.value
    }

    fun getPedidosConCantidadMayorACero(): List<Pedido> {
        val nuevaLista: MutableList<Pedido> = _pedidos.value ?: mutableListOf<Pedido>()
        return nuevaLista.filter { pedido -> pedido.cantidad!! > 0 }
    }

    private fun removerPedidoDelCarrito(productoPedido: Pedido): MutableList<Pedido> {
        val nuevaLista: MutableList<Pedido> = _pedidos.value ?: mutableListOf<Pedido>()
        nuevaLista.remove(productoPedido)
        return nuevaLista
    }

    private fun updatePedidos(pedidosNuevos: MutableList<Pedido>) {
        _pedidos.value = pedidosNuevos
    }


    fun get_compra(): Compra {
        return this.compra
    }

    fun get_precio_platos_carrito(): Double {
        if (pedidos.value?.isEmpty() == true) return 0.00
        var total = 0.0
        _pedidos.value?.forEach { p -> total += p.precio!! * (p.cantidad!!.toDouble()) }
        return  total
    }
}