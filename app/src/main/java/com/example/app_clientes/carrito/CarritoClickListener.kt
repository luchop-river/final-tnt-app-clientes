package com.example.app_clientes.carrito

import com.example.app_clientes.modelos.Pedido

interface CarritoClickListener {
    fun onIncrementarCantidadProductoClicked(producto: Pedido)
    fun onRestarCantidadProductoClicked(producto: Pedido)
}