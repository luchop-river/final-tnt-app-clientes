package com.example.app_clientes.carrito

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.app_clientes.modelos.Pedido

class CarritoViewModelFactory (private val platos: MutableList<Pedido>): ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CarritoViewModel( platos ) as T
    }
}