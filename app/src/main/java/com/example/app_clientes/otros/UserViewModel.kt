package com.example.app_clientes.otros

import androidx.lifecycle.ViewModel
import com.example.app_clientes.modelos.User

class UserViewModel: ViewModel() {
    lateinit var usuario: User
    var token: String = ""
    var favoritos: MutableList<String> = mutableListOf()

    fun borrar_favorito(nombre:String){
        var index = favoritos.indexOf(nombre)
        favoritos.removeAt(index)
    }

    fun add_favorito(nombre:String){
        favoritos.add(nombre)
    }

    fun limpiar_favoritos(){
        this.favoritos.clear()
    }
}