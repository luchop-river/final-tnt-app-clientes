package com.example.app_clientes.otros

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.app_clientes.modelos.Compra
import com.example.app_clientes.modelos.Pedido
import com.example.app_clientes.modelos.User

//class CompraViewModel(private var compra_a_realizar: Compra) {
class CompraViewModel: ViewModel() {
    private var _compra: MutableLiveData<Compra> = MutableLiveData()
    val compra: LiveData<Compra>
        get() = _compra

    private var pedidos: List<Pedido> = listOf<Pedido>()

    lateinit var user: User

    /*init {
        _compra.postValue(compra_a_realizar)
    }*/
    fun set_compra(compra: Compra) {
        this._compra.value = compra
    }

    fun get_compra(): Compra {
        return this.compra.value !!
    }

    fun set_pedidos(lista_pedidos: List<Pedido>) {
        this.pedidos = lista_pedidos
    }

    fun get_pedidos(): List<Pedido> {
        return this.pedidos
    }
}